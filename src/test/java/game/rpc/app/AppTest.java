package game.rpc.app;

import game.rpc.game.action.ActionStore;
import game.rpc.game.player.Player;

import static org.junit.Assert.*;

import org.junit.Test;

public class AppTest {

    @Test
    public void app_100turnGame_P1Wins(){
        int turns = 10;
        Player player1  =
                new Player(
                        "Player1",
                        () -> ActionStore.SCISSORS
                );

        Player player2 =
                new Player(
                        "Player2",
                        () -> ActionStore.PAPER
                );

        App sut = new App(player1, player2);
        sut.run((increment) -> increment < turns);

        assertEquals(turns, player1.getScore());
        assertEquals(0, player2.getScore());
        assertTrue(player1.getScore() > player2.getScore());
    }

    @Test
    public void app_100turnGame_P2Wins(){
        int turns = 100;
        Player player1  =
                new Player(
                        "Player1",
                        () -> ActionStore.SCISSORS
                );

        Player player2 =
                new Player(
                        "Player2",
                        () -> ActionStore.ROCK
                );

        App sut = new App(player1, player2);
        sut.run((increment) -> increment < turns);

        assertEquals(turns, player2.getScore());
        assertEquals(0, player1.getScore());
        assertTrue(player2.getScore() > player1.getScore());
    }

    @Test
    public void app_100turnGame_DrawGame(){
        int turns = 100;
        Player player1  =
                new Player(
                        "Player1",
                        () -> ActionStore.ROCK
                );

        Player player2 =
                new Player(
                        "Player2",
                        () -> ActionStore.ROCK
                );

        App sut = new App(player1, player2);
        sut.run((increment) -> increment < turns);

        assertEquals(0, player1.getScore());
        assertEquals(0, player2.getScore());
        assertTrue(player1.getScore() == player2.getScore());
    }
}
