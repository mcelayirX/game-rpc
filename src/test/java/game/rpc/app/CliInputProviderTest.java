package game.rpc.app;

import game.rpc.game.action.ActionStore;
import game.rpc.app.exception.GameInterruptException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

import static org.junit.Assert.*;
import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

import java.io.ByteArrayInputStream;

import java.io.InputStream;

public class CliInputProviderTest {

    @Rule
    public final TextFromStandardInputStream testInputStream
            = emptyStandardInputStream();


    @Test
    public void cliInput_ActionRock() {
        InputStream in = new ByteArrayInputStream("1".getBytes());
        CliInputProvider sut = new CliInputProvider(in);
        assertEquals(ActionStore.ROCK, sut.getAction());
    }

    @Test
    public void cliInput_ActionPaper() {
        InputStream in = new ByteArrayInputStream("2".getBytes());
        CliInputProvider sut = new CliInputProvider(in);
        assertEquals(ActionStore.PAPER, sut.getAction());
    }

    @Test
    public void cliInput_ActionScissors() {
        InputStream in = new ByteArrayInputStream("3".getBytes());
        CliInputProvider sut = new CliInputProvider(in);
        assertEquals(ActionStore.SCISSORS, sut.getAction());
    }

    @Test
    public void cliInput_CorrectActionAfterInvalidInput() {
        testInputStream.provideLines("5", "4", "3");
        CliInputProvider sut = new CliInputProvider(System.in);
        assertEquals(ActionStore.SCISSORS, sut.getAction());
    }

    @Test(expected = GameInterruptException.class)
    public void cliInput_GameInterruptException() {
        ByteArrayInputStream in = new ByteArrayInputStream("Q".getBytes());
        new CliInputProvider(in).getAction();
    }

}
