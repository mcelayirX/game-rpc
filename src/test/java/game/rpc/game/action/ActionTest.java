package game.rpc.game.action;

import static org.junit.Assert.*;

import game.rpc.game.Result;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class ActionTest {

    @Test
    public void action_Rock() {
        assertEquals("ROCK", ActionStore.ROCK.toString());
        assertEquals("ROCK", ActionStore.ROCK.getName());
        Assert.assertEquals(Result.WIN, ActionStore.ROCK.beats(ActionStore.SCISSORS));
        Assert.assertEquals(Result.LOSE, ActionStore.ROCK.beats(ActionStore.PAPER));
        Assert.assertEquals(Result.DRAW, ActionStore.ROCK.beats(ActionStore.ROCK));
    }

    @Test
    public void action_Paper() {
        assertEquals("PAPER", ActionStore.PAPER.toString());
        assertEquals("PAPER", ActionStore.PAPER.getName());
        Assert.assertEquals(Result.LOSE, ActionStore.PAPER.beats(ActionStore.SCISSORS));
        Assert.assertEquals(Result.DRAW, ActionStore.PAPER.beats(ActionStore.PAPER));
        Assert.assertEquals(Result.WIN, ActionStore.PAPER.beats(ActionStore.ROCK));
    }

    @Test
    public void action_Scissors() {
        assertEquals("SCISSORS", ActionStore.SCISSORS.toString());
        assertEquals("SCISSORS", ActionStore.SCISSORS.getName());
        Assert.assertEquals(Result.DRAW, ActionStore.SCISSORS.beats(ActionStore.SCISSORS));
        Assert.assertEquals(Result.WIN, ActionStore.SCISSORS.beats(ActionStore.PAPER));
        Assert.assertEquals(Result.LOSE, ActionStore.SCISSORS.beats(ActionStore.ROCK));
    }
}
