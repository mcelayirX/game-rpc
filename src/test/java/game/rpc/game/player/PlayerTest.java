package game.rpc.game.player;

import game.rpc.game.action.Action;
import game.rpc.game.action.ActionStore;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

public class PlayerTest {

    @Test
    public void player_getName(){
        Player sut = new Player("test", () -> null);

        assertEquals("test", sut.getName());
    }

    @Test
    public void player_takeAction(){
        Player sut = new Player("test", new ActionDecider() {
            private int counter = 0;
            @Override
            public Action nextAction() {
                counter++;
                if(counter == 1) {
                    return ActionStore.ROCK;
                }else if(counter == 2){
                    return ActionStore.PAPER;
                }else{
                    return ActionStore.SCISSORS;
                }
            }
        });

        Assert.assertEquals(ActionStore.ROCK, sut.takeAction());
        Assert.assertEquals(ActionStore.ROCK, sut.getLastAction());
        Assert.assertEquals(ActionStore.PAPER, sut.takeAction());
        Assert.assertEquals(ActionStore.PAPER, sut.getLastAction());
        Assert.assertEquals(ActionStore.SCISSORS, sut.takeAction());
        Assert.assertEquals(ActionStore.SCISSORS, sut.getLastAction());
    }

    @Test
    public void player_score(){
        Player sut = new Player("test", () -> null);

        assertEquals(0, sut.getScore());
        sut.incrementScore(1);
        assertEquals(1, sut.getScore());
        sut.incrementScore(3);
        assertEquals(4, sut.getScore());
    }
}
