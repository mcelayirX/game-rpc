package game.rpc.game.player;

import game.rpc.game.action.ActionStore;
import org.junit.Assert;
import org.junit.Test;

public class HumanPlayerActionDeciderTest {

    @Test
    public void nextAction_InputProvider(){
        HumanPlayerActionDecider sut = new HumanPlayerActionDecider(() -> ActionStore.ROCK);
        Assert.assertEquals(ActionStore.ROCK, sut.nextAction());
    }
}
