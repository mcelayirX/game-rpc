package game.rpc.game.player;

import game.rpc.game.action.ActionStore;
import game.rpc.game.action.Action;
import org.junit.Test;

import java.util.Arrays;
import static org.junit.Assert.*;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ComPlayerActionDeciderTest {

    @Test
    public void nextAction_randomAction() {

        int length = 100000;
        Action[] actions = new Action[length];
        ComPlayerActionDecider sut = new ComPlayerActionDecider();
        for (int i = 0; i < length; i++) {
            actions[i] = sut.nextAction();
        }

       Map<Action, Long> occurenceMap = Arrays.asList(actions)
                .stream()
                .collect(Collectors
                        .groupingBy(
                                Function.identity(),
                                Collectors.counting()
                        )
                );


        assertEquals(true, occurenceMap.containsKey(ActionStore.ROCK));
        assertEquals(true, occurenceMap.containsKey(ActionStore.PAPER));
        assertEquals(true, occurenceMap.containsKey(ActionStore.SCISSORS));
    }
}
