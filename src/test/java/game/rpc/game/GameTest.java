package game.rpc.game;

import static org.junit.Assert.*;

import game.rpc.game.action.Action;
import game.rpc.game.action.ActionStore;
import game.rpc.game.player.ActionDecider;
import game.rpc.game.player.Player;
import org.junit.Assert;
import org.junit.Test;


public class GameTest {

    @Test
    public void game_p1Wins(){

        Player p1 = new Player("p1", () -> ActionStore.ROCK);

        Player p2 = new Player("p2", () -> ActionStore.SCISSORS);

        Game sut = new Game(p1, p2);
        sut.playTurn();
        Assert.assertEquals(p1, sut.getWinner().get());
    }

    @Test
    public void game_p2Wins(){

        Player p1 = new Player("p1", () -> ActionStore.ROCK);

        Player p2 = new Player("p2", () -> ActionStore.PAPER);

        Game sut = new Game(p1, p2);
        sut.playTurn();
        Assert.assertEquals(p2, sut.getWinner().get());
    }

    @Test
    public void game_draw(){

        Player p1 = new Player("p1", () -> ActionStore.ROCK);

        Player p2 = new Player("p2", () -> ActionStore.ROCK);

        Game sut = new Game(p1, p2);
        sut.playTurn();
        assertEquals(false, sut.getWinner().isPresent());
        assertEquals(1, sut.getDrawCount());
    }

    @Test
    public void game_drawsEqualToPlayerScore(){

        Player p1 = new Player("p1", new ActionDecider() {
            private int counter = 0;

            @Override
            public Action nextAction() {
                if(counter == 0){
                    counter++;
                    return ActionStore.PAPER;
                }
                return ActionStore.ROCK;
            }
        });

        Player p2 = new Player("p2", () -> ActionStore.ROCK);

        Game sut = new Game(p1, p2);
        sut.playTurn();
        sut.playTurn();
        Assert.assertEquals(p1, sut.getWinner().get());
        assertEquals(1, sut.getDrawCount());
    }

    @Test
    public void game_getPlayers(){

        Player p1 = new Player("p1", () -> null);

        Player p2 = new Player("p2", () -> null);

        Game sut = new Game(p1, p2);
        Assert.assertArrayEquals(new Player[]{p1, p2}, sut.getPlayers());
    }
}
