package game.rpc;

import game.rpc.app.CliInputProvider;
import game.rpc.game.player.ComPlayerActionDecider;
import game.rpc.game.player.Player;
import game.rpc.app.App;
import game.rpc.game.player.HumanPlayerActionDecider;

public class Main {

    public static void main(String[] args) {
        Player comPlayer =
                new Player(
                        "Computer",
                        new ComPlayerActionDecider()
                );

        Player humanPlayer =
                new Player(
                        "User",
                        new HumanPlayerActionDecider(new CliInputProvider(System.in))
                );
        App app = new App(comPlayer, humanPlayer);
        app.run((increment) -> true);
    }
}
