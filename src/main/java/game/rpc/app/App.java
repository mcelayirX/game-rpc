package game.rpc.app;

import game.rpc.app.exception.GameInterruptException;
import game.rpc.game.Game;
import game.rpc.game.player.Player;

import java.util.*;
import java.util.function.Function;

public class App {

    private final Player comPlayer;
    private final Player humanPlayer;
    private final Game game;

    public App(Player comPlayer, Player humanPlayer) {
        this.comPlayer = comPlayer;
        this.humanPlayer = humanPlayer;
        this.game = new Game(comPlayer, humanPlayer);
    }

    public void run(Function<Integer, Boolean> condition) {
        int i = 0;
        while (condition.apply(i)) {
            try {
                playTurn();
                i++;
            } catch (GameInterruptException ex) {
                System.out.println("User decided to stop the game");
                logScores(game);
                Optional<Player> winner = game.getWinner();
                System.out.println("Winner of the game: " + getName(winner));
                System.exit(0);
            }
        }
    }

    private void playTurn() {
        Optional<Player> winner = game.playTurn();
        Optional<Player> leader = game.getWinner();

        System.out.println(
                "Moves -> "
                        + humanPlayer.getName()
                        + " : "
                        + humanPlayer.getLastAction()
                        + " - Com Player: "
                        + comPlayer.getLastAction());
        System.out.println("Winner of the round: " + getName(winner));
        System.out.println("Leader of the game: " + getName(leader));

        logScores(game);
    }

    private void logScores(Game game) {
        System.out.println("=======");
        System.out.println("Scores:");
        Arrays.stream(game.getPlayers())
                .forEach(
                        player ->
                                System.out.println(
                                        "Player: " + player.getName() + " - Score: " + player.getScore()));
        System.out.println("Draws: " + game.getDrawCount());
        System.out.println("=======");
    }

    private String getName(Optional<Player> player) {
        return player.map(value -> value.getName()).orElse("None");
    }
}
