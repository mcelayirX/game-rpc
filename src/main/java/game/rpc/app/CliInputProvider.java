package game.rpc.app;

import game.rpc.app.exception.GameInterruptException;
import game.rpc.game.action.ActionStore;
import game.rpc.game.action.Action;
import game.rpc.game.player.HumanInputProvider;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CliInputProvider implements HumanInputProvider {

    private static final Map<String, Action> actions =
            new HashMap() {
                {
                    put("1", ActionStore.ROCK);
                    put("2", ActionStore.PAPER);
                    put("3", ActionStore.SCISSORS);
                }
            };

    private final Scanner scanner;

    public CliInputProvider(InputStream inputStream) {
        this.scanner = new Scanner(inputStream);
    }

    @Override
    public Action getAction() {
        displayMenu();
        String input = scanner.nextLine();

        if (input.contentEquals("Q") || input.contentEquals("q")) {
            throw new GameInterruptException();
        } else if (actions.containsKey(input)) {
            return actions.get(input);
        }

        System.out.println("Invalid input");
        return getAction();
    }

    private void displayMenu(){
        actions.entrySet().stream()
                .forEach(
                        item ->
                                System.out.println(item.getKey() + " - " + item.getValue().toString()));
        System.out.println("Q - Exit");
        System.out.println("Make your choice: ");
    }
}
