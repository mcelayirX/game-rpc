package game.rpc.game;

public enum Result {
  DRAW,
  WIN,
  LOSE,
}
