package game.rpc.game.player;

import game.rpc.game.action.Action;

public class Player implements Comparable<Player> {

  private final String name;
  private final ActionDecider actionDecider;

  private int score = 0;
  private Action lastAction = null;

  public Player(String name, ActionDecider actionDecider) {
    this.name = name;
    this.actionDecider = actionDecider;
  }

  @Override
  public int compareTo(Player o) {
    return this.score - o.score;
  }

  public Action takeAction() {
    lastAction = actionDecider.nextAction();
    return lastAction;
  }

  public String getName() {
    return name;
  }

  public int getScore() {
    return score;
  }

  public void incrementScore(int score) {
    this.score += score;
  }

  public Action getLastAction() {
    return lastAction;
  }
}
