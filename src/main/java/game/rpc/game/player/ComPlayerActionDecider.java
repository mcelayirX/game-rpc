package game.rpc.game.player;

import game.rpc.game.action.Action;
import game.rpc.game.action.ActionStore;

import java.util.Random;

public class ComPlayerActionDecider implements ActionDecider {

    private static final Action[] possibleActions = new Action[]{ActionStore.SCISSORS, ActionStore.PAPER, ActionStore.ROCK};

    private final Random random = new Random();

    @Override
    public Action nextAction() {
        return possibleActions[random.nextInt(possibleActions.length)];
    }
}
