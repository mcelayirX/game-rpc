package game.rpc.game.player;

import game.rpc.game.action.Action;

public class HumanPlayerActionDecider implements ActionDecider{

    private final HumanInputProvider humanInputProvider;

    public HumanPlayerActionDecider(HumanInputProvider humanInputProvider) {
        this.humanInputProvider = humanInputProvider;
    }

    @Override
    public Action nextAction() {
        return humanInputProvider.getAction();
    }
}
