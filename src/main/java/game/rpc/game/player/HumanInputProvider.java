package game.rpc.game.player;

import game.rpc.game.action.Action;

public interface HumanInputProvider {

    Action getAction();
}
