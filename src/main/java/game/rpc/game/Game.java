package game.rpc.game;

import game.rpc.game.player.Player;
import game.rpc.game.action.Action;

import java.util.Optional;

public class Game {

  private static final Optional<Player> drawPlayer = Optional.empty();

  private final Player player1;
  private final Player player2;
  private final Player[] players;

  private int drawCount = 0;

  public Game(Player player1, Player player2) {
    this.player1 = player1;
    this.player2 = player2;
    this.players = new Player[] {player1, player2};
  }

  public Optional<Player> playTurn() {
    Action player1Action = player1.takeAction();
    Action player2Action = player2.takeAction();

    Result result = player1Action.beats(player2Action);
    if (result == Result.DRAW) {
      drawCount++;
      return drawPlayer;
    } else if (result == Result.WIN) {
      player1.incrementScore(1);
      return Optional.of(player1);
    } else {
      player2.incrementScore(1);
      return Optional.of(player2);
    }
  }

  public Optional<Player> getWinner() {
    int result = player1.compareTo(player2);
    if (result == 0) {
      return drawPlayer;
    } else if (result > 0) {
      return Optional.of(player1);
    } else {
      return Optional.of(player2);
    }
  }

  public Player[] getPlayers() {
    return players;
  }

  public int getDrawCount() {
    return drawCount;
  }
}
