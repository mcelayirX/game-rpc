package game.rpc.game.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Paper extends Action {

    protected Paper() {
        super("PAPER");
    }

    @Override
    protected List<Action> getLowerActions() {
        return new ArrayList<>(Arrays.asList(ActionStore.ROCK));
    }
}
