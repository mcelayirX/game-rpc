package game.rpc.game.action;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Rock extends Action {

  protected Rock() {
    super("ROCK");
  }

  @Override
  protected List<Action> getLowerActions() {
    return new ArrayList<>(Arrays.asList(ActionStore.SCISSORS));
  }
}
