package game.rpc.game.action;

import game.rpc.game.Result;

import java.util.List;

public abstract class Action {

    private final String name;

    public Action(String name) {
        this.name = name;
    }

    protected abstract List<Action> getLowerActions();

    public Result beats(Action action) {
        if (this == action) {
            return Result.DRAW;
        } else if (getLowerActions().contains(action)) {
            return Result.WIN;
        }
        return Result.LOSE;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
