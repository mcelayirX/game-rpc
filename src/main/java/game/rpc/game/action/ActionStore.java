package game.rpc.game.action;

public class ActionStore {

  public static Action ROCK = new Rock();
  public static Action PAPER = new Paper();
  public static Action SCISSORS = new Scissors();
}
