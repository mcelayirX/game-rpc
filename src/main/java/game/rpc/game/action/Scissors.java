package game.rpc.game.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Scissors extends Action {

  protected Scissors() {
    super("SCISSORS");
  }

  @Override
  protected List<Action> getLowerActions() {
    return new ArrayList<>(Arrays.asList(ActionStore.PAPER));
  }
}
